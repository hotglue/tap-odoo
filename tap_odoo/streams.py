"""Stream type classes for tap-odoo."""

from pathlib import Path
from typing import Any, Dict, Iterable, List, Optional, Union

from singer_sdk import typing as th

from tap_odoo.client import OdooStream


class ProductStream(OdooStream):
    """Define product stream."""

    name = "products"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "product.product"


class ProductUomStream(OdooStream):
    """Define product stream."""

    name = "products_uom"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "uom.uom"


class CustomerStream(OdooStream):
    """Define user stream."""

    name = "customers"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "res.partner"


class PartnerStream(OdooStream):
    """Define user stream."""

    name = "partners"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "res.partner"


class OrderStream(OdooStream):
    """Define sale orders stream."""

    name = "sale_orders"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "sale.order"


class SaleOrderLineStream(OdooStream):
    """Define sale order lines stream."""

    name = "sale_order_line"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "sale.order.line"


class PurchaseStream(OdooStream):
    """Define purchase orders stream."""

    name = "purchase_orders"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "purchase.order"


class PurchaseLineStream(OdooStream):
    """Define purchases stream."""

    name = "purchase_order_lines"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "purchase.order.line"


class StockStream(OdooStream):
    """Define stock stream."""

    name = "stock"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "stock.quant"


class WarehouseStream(OdooStream):
    """Define warehouse line stream."""

    name = "warehouse"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "stock.warehouse"


class LocationStream(OdooStream):
    """Define location line stream."""

    name = "location"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "stock.location"


class ProductsSupplierStream(OdooStream):
    """Define location line stream."""

    name = "product_suppliers"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "product.supplierinfo"


class CompaniesStream(OdooStream):
    """Define location line stream."""

    name = "companies"
    primary_keys = ["id"]
    replication_key = None
    endpoint = "res.company"


class UsersStream(OdooStream):
    """Define location line stream."""

    name = "users"
    primary_keys = ["id"]
    replication_key = None
    endpoint = "res.users"


class AccountsStream(OdooStream):
    """Define location line stream."""

    name = "accounts"
    primary_keys = ["id"]
    replication_key = None
    endpoint = "account.account"


class InvoicesStream(OdooStream):
    """Define Invoices / Vendor bills stream."""

    name = "invoices_bills"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "account.move"

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {"invoice_id": record["id"]}


class BomStream(OdooStream):
    """Define Bill of materials stream."""

    name = "bom"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "mrp.bom"

class BomLinesStream(OdooStream):
    """Define Line of Bill of materials stream."""

    name = "bom_lines"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "mrp.bom.line"

class InvoiceLinesStream(OdooStream):
    """Define Invoices / Vendor bills stream."""

    name = "invoice_bill_lines"
    primary_keys = ["id"]
    path = "{invoice_id}"
    replication_key = None
    endpoint = "account.move.line"
    parent_stream_type = InvoicesStream

    @property
    def filters(self):
        #Context is not available. Have to rely on partitions
        if "invoice_id" in self.partitions[-1]:
            invoice_id = self.partitions[-1]["invoice_id"]
            return ["move_id","=",invoice_id]
        return None

class InvoiceLineAllStream(OdooStream):
    """Define Invoices / Vendor bills stream."""

    name = "invoice_lines_all"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "account.move.line"

class StockMoveStream(OdooStream):
    """Define Stock movement stream."""

    name = "stock_move"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "stock.move"

class PricelistItemsStream(OdooStream):
    """Define pricelist items stream."""

    name = "pricelist_items"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "product.pricelist.item"

class SupplierStream(OdooStream):
    """Define location line stream."""

    name = "suppliers"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "res.partner"

    @property
    def filters(self):
        return ["supplier_rank",">",0]
class ProductTemplateStream(OdooStream):
    """Define location line stream."""
    name = "product_templates"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "product.template" 
    
    @property
    def ignore_list(self):
        ignore_list = [
            "is_published",
            "bom_line_ids",
            "bom_ids",
            "bom_count"
        ]
        for property in self.selected_properties:
            if property.startswith("website_"):
                ignore_list.append(property)
        return ignore_list
    
class ProductPackagingStream(OdooStream):
    """Define Stock movement stream."""

    name = "product_packaging"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "product.packaging"

class PosOrderStream(OdooStream):
    """Define pos order stream."""

    name = "pos_orders"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "pos.order"

class PosOrderLineStream(OdooStream):
    """Define pos order stream."""

    name = "pos_order_line"
    primary_keys = ["id"]
    replication_key = "write_date"
    endpoint = "pos.order.line"
